package pond.common.abs;

/**
 * @author ed
 */
public interface Config<V> {

    public void config(V v);
}
